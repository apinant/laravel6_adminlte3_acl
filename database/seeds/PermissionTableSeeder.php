<?php

use Illuminate\Database\Seeder;
use App\Models\Permission;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $permissions = [
        // 'role-list',
        // 'role-create',
        // 'role-edit',
        // 'role-delete',
        // 'user-list',
        // 'user-create',
        // 'user-edit',
        // 'user-delete',
        // ];

        $permissions = [
            [
                'name' => 'admin.users.index',
                'guard_name' => 'web',
                'route_controller' => 'UserController',
                'route_action' => 'index',
                'route_path' =>  'App\Http\Controllers\Admin\UserController@index',
                'route_method_uri' => 'GET|HEAD,admin/users',
                'description' => 'users-list',
                'active' => '1',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
            [
                'name' => 'admin.users.create',
                'guard_name' => 'web',
                'route_controller' => 'UserController',
                'route_action' => 'create',
                'route_path' =>  'App\Http\Controllers\Admin\UserController@create',
                'route_method_uri' => 'GET|HEAD,admin/users/create',
                'description' => 'users-create',
                'active' => '1',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
            [
                'name' => 'admin.users.store',
                'guard_name' => 'web',
                'route_controller' => 'UserController',
                'route_action' => 'store',
                'route_path' =>  'App\Http\Controllers\Admin\UserController@store',
                'route_method_uri' => 'POST,admin/users',
                'description' => 'users-store',
                'active' => '1',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
            [
                'name' => 'admin.users.show',
                'guard_name' => 'web',
                'route_controller' => 'UserController',
                'route_action' => 'show',
                'route_path' =>  'App\Http\Controllers\Admin\UserController@show',
                'route_method_uri' => 'GET|HEAD,admin/users/{user}',
                'description' => 'users-show',
                'active' => '1',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
            [
                'name' => 'admin.users.edit',
                'guard_name' => 'web',
                'route_controller' => 'UserController',
                'route_action' => 'edit',
                'route_path' =>  'App\Http\Controllers\Admin\UserController@edit',
                'route_method_uri' => 'GET|HEAD,admin/users/{user}/edit',
                'description' => 'users-edit',
                'active' => '1',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
            [
                'name' => 'admin.users.update',
                'guard_name' => 'web',
                'route_controller' => 'UserController',
                'route_action' => 'update',
                'route_path' =>  'App\Http\Controllers\Admin\UserController@update',
                'route_method_uri' => 'PUT|PATCH,admin/users/{user}',
                'description' => 'users-update',
                'active' => '1',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
            [
                'name' => 'admin.users.destroy',
                'guard_name' => 'web',
                'route_controller' => 'UserController',
                'route_action' => 'destroy',
                'route_path' =>  'App\Http\Controllers\Admin\UserController@destroy',
                'route_method_uri' => 'DELETE,admin/users/{user}',
                'description' => 'users-delete',
                'active' => '1',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
            [
                'name' => 'admin.roles.index',
                'guard_name' => 'web',
                'route_controller' => 'RoleController',
                'route_action' => 'index',
                'route_path' =>  'App\Http\Controllers\Admin\RoleController@index',
                'route_method_uri' => 'GET|HEAD,admin/roles',
                'description' => 'roles-list',
                'active' => '1',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
            [
                'name' => 'admin.roles.create',
                'guard_name' => 'web',
                'route_controller' => 'RoleController',
                'route_action' => 'create',
                'route_path' =>  'App\Http\Controllers\Admin\RoleController@create',
                'route_method_uri' => 'GET|HEAD,admin/roles/create',
                'description' => 'roles-create',
                'active' => '1',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
            [
                'name' => 'admin.roles.store',
                'guard_name' => 'web',
                'route_controller' => 'RoleController',
                'route_action' => 'store',
                'route_path' =>  'App\Http\Controllers\Admin\RoleController@store',
                'route_method_uri' => 'POST,admin/roles',
                'description' => 'roles-store',
                'active' => '1',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
            [
                'name' => 'admin.roles.show',
                'guard_name' => 'web',
                'route_controller' => 'RoleController',
                'route_action' => 'show',
                'route_path' =>  'App\Http\Controllers\Admin\RoleController@show',
                'route_method_uri' => 'GET|HEAD,admin/roles/{role}',
                'description' => 'roles-show',
                'active' => '1',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
            [
                'name' => 'admin.roles.edit',
                'guard_name' => 'web',
                'route_controller' => 'RoleController',
                'route_action' => 'edit',
                'route_path' =>  'App\Http\Controllers\Admin\RoleController@edit',
                'route_method_uri' => 'GET|HEAD,admin/roles/{role}/edit',
                'description' => 'roles-edit',
                'active' => '1',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
            [
                'name' => 'admin.roles.update',
                'guard_name' => 'web',
                'route_controller' => 'RoleController',
                'route_action' => 'update',
                'route_path' =>  'App\Http\Controllers\Admin\RoleController@update',
                'route_method_uri' => 'PUT|PATCH,admin/roles/{role}',
                'description' => 'roles-update',
                'active' => '1',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
            [
                'name' => 'admin.roles.destroy',
                'guard_name' => 'web',
                'route_controller' => 'RoleController',
                'route_action' => 'destroy',
                'route_path' =>  'App\Http\Controllers\Admin\RoleController@destroy',
                'route_method_uri' => 'DELETE,admin/roles/{role}',
                'description' => 'roles-delete',
                'active' => '1',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
            [
                'name' => 'admin.permissions.index',
                'guard_name' => 'web',
                'route_controller' => 'PermissionController',
                'route_action' => 'index',
                'route_path' =>  'App\Http\Controllers\Admin\PermissionController@index',
                'route_method_uri' => 'GET|HEAD,admin/permissions',
                'description' => 'permissions-list',
                'active' => '1',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
            [
                'name' => 'admin.permissions.generator',
                'guard_name' => 'web',
                'route_controller' => 'PermissionController',
                'route_action' => 'generator',
                'route_path' =>  'App\Http\Controllers\Admin\PermissionController@generator',
                'route_method_uri' => 'PATCH,admin/permissions/generator',
                'description' => 'permissions-generator',
                'active' => '1',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ],
        ];
        foreach ($permissions as $permission) {
            Permission::create($permission);
        }
    }
}
