<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Permission;
use Illuminate\Http\Request;
use Spatie\Permission\Guard;

class PermissionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $permissions = Permission::where('active', 1)->get();
        $permissions_group_controller = $permissions->groupBy('route_controller');
        return view('admin.permissions.index', compact('permissions_group_controller'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Permission  $permission
     * @return \Illuminate\Http\Response
     */
    public function show(Permission $permission)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Permission  $permission
     * @return \Illuminate\Http\Response
     */
    public function edit(Permission $permission)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Permission  $permission
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Permission $permission)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Permission  $permission
     * @return \Illuminate\Http\Response
     */
    public function destroy(Permission $permission)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function generator(Request $request)
    {
        foreach (\Route::getRoutes() as $Route) {
            if($Route->getName()){
                // check exit row
                $havePermission = Permission::where('name', $Route->getName())->count();
                if(! $havePermission ){
                    $str = $Route->getActionName();

                    // Filter call controller in app
                    if(strpos($str, "App\\Http\\Controllers") !== 0) continue;
                    if(strpos($str, "App\\Http\\Controllers\\Auth") === 0) continue;

                    $con_act = explode( "\\", $str );
                    $con_act = end($con_act);
                    list($controller, $action) = explode( "@", $con_act );

                    //Get method HEAD|GET|POST|PUT
                    $methods = $Route->methods();
                    $method = implode("|", $methods);
                    $guard_name = Guard::getDefaultName(static::class);

                    $roles_new[] = array(
                        'name' => $Route->getName(),
                        'guard_name' => $guard_name,
                        'route_controller' => $controller,
                        'route_action' => $action,
                        'route_path' =>  $str,
                        'route_method_uri' => $method.",".$Route->uri,
                        'description' => $Route->uri,
                        'active' => '1',
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s'),
                    );
                }
            }
        }
//        dd($roles_new);
        if(isset($roles_new)){
            Permission::insert($roles_new);
        }
        return redirect()->route('admin.permissions.index')
            ->with('success','Permisson updated successfully');
    }
}
