## Laravel 6 / AdminLTE 3 / Permission and Roles

### Installation

- In your terminal 
``` bash
composer install
```
- Copy `.env.example` to `.env` and updated the configurations (mainly the database configuration)
- In your terminal 
``` bash
php artisan key:generate
```
- Create the database tables and seed the roles and users tables. 
Run
``` bash
php artisan migrate --seed
```

### Usage

To start, register as a user or log in using one of the default users:

Admin type - `admin@gmail.com` with the password `secret`

### Alternatives
- **[Associate users with permissions and roles](https://github.com/spatie/laravel-permission)**
- **[ColorlibHQ/AdminLTE v3.0.2](https://github.com/ColorlibHQ/AdminLTE)**
