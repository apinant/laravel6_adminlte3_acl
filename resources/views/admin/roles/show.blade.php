@extends('layouts.adminlte3')
@section('title', 'Show Role')
@section('content-header')
    <div class="row mb-2">
        <div class="col-sm-6">
            <h1 class="m-0 text-dark">Show Role</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
                <li class="breadcrumb-item"><a href="{{ route('admin.roles.index') }}">Roles</a></li>
                <li class="breadcrumb-item active">Show</li>
            </ol>
        </div><!-- /.col -->
    </div><!-- /.row -->
@endsection

@section('content')
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Show Role</h3>
            <div class="card-tools"><a class="btn btn-primary" href="{{ route('admin.roles.index') }}"><i class="fa fa-angle-left"></i> Back to list</a></div>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <div class="card-body">

            <div class="row">
                <div class="col-xs-6 col-sm-6 col-md-6">
                    <div class="form-group">
                        <label>Name:</label>
                        {!! Form::text('name', $role->name, array('placeholder' => 'Name','class' => 'form-control', 'disabled' => 'true')) !!}
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <label>Permissions:</label>
                        <br>
                        @if(!empty($rolePermissions))
                            @foreach($rolePermissions as $v)
                                <span class="label label-success">{{ $v->name }},</span>
                            @endforeach
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <!-- /.card-body -->
    </div>
    <!-- /.card -->
@endsection
