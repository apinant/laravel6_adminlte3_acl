@extends('layouts.adminlte3')
@section('title', 'Users')
@section('content-header')
    <div class="row mb-2">
        <div class="col-sm-6">
            <h1 class="m-0 text-dark">Permission Management</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
                <li class="breadcrumb-item active">Permissions</li>
            </ol>
        </div><!-- /.col -->
    </div><!-- /.row -->
@endsection

@section('content')
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">
                @can('admin.permissions.generator')
                    {!! Form::open(array('route' => 'admin.permissions.generator','method'=>'PATCH')) !!}
                    <button type="submit" class="btn btn-success" > Update permission</button>
                    {!! Form::close() !!}
                @endcan
            </h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            @php ($style = ['primary', 'success', 'warning', 'danger'])
            @php ($style_no = 0)
            @foreach ($permissions_group_controller as $controller => $permissions)
            <div class="card card-outline card-{{$style[$style_no]}}">
                <div class="card-header">
                    <h3 class="card-title">{{ $controller }}</h3>
                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                        </button>
                    </div>
                    <!-- /.card-tools -->
                </div>
                <!-- /.card-header -->
                <div class="card-body table-responsive p-0">
                    <table class="table table-hover text-nowrap">
                        <thead>
                        <tr>
                            <th>No</th>
                            <th>Name</th>
                            <th>Method</th>
                            <th>URI</th>
                            <th>Description</th>
                        </tr>
                        </thead>
                        <tbody>
                        @php($i = 0)
                        @foreach ($permissions as $key => $permission)
                            <tr>
                                <td>{{ ++$i }}</td>
                                <td>{{ $permission->name }}</td>
                                <td>{{ $permission->route_action }}</td>
                                <td>{{ $permission->route_method_uri }}</td>
                                <td>{{ $permission->description }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
                @php( ++ $style_no )
                @if($style_no == 4)
                    @php($style_no = 0)
                @endif
            @endforeach
        </div>
        <!-- /.card-body -->
    </div>
    <!-- /.card -->
@endsection
