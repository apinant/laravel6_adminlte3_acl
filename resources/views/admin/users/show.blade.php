@extends('layouts.adminlte3')
@section('title', 'Show User')
@section('content-header')
    <div class="row mb-2">
        <div class="col-sm-6">
            <h1 class="m-0 text-dark">Show User</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
                <li class="breadcrumb-item"><a href="{{ route('admin.users.index') }}">Users</a></li>
                <li class="breadcrumb-item active">Show User</li>
            </ol>
        </div><!-- /.col -->
    </div><!-- /.row -->
@endsection
@section('content')
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Show User</h3>
            <div class="card-tools"><a class="btn btn-primary" href="{{ route('admin.users.index') }}"><i class="fa fa-angle-left"></i> Back to list</a></div>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <div class="card-body">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Name:</label>
                        {!! Form::text('name', $user->name, array('placeholder' => 'Name','class' => 'form-control', 'disabled' => 'true')) !!}
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Email:</label>
                        {!! Form::text('name', $user->email, array('placeholder' => 'Email','class' => 'form-control', 'disabled' => 'true')) !!}
                    </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12">
                    <div class="form-group">
                        <label>Roles:</label>
                        <div>
                        @if(!empty($user->getRoleNames()))
                            @foreach($user->getRoleNames() as $v)
                                <a href="#" class="btn bg-gradient-danger btn-flat">{{ $v }}</a>
                            @endforeach
                        @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- /.card-body -->
    </div>
    <!-- /.card -->
@endsection
